package com.example.sorteio;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextView textoCalculo;
    EditText editText;
    EditText editText2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textoCalculo = (TextView) findViewById(R.id.textView2);
        editText = findViewById(R.id.editText);
        editText2 = findViewById(R.id.editText2);

        String a = editText.getText().toString();
        String b = editText2.getText().toString();

        int x = Integer.parseInt(a);
        int y = Integer.parseInt(b);

        Random sorteio = new Random();
        int valor = sorteio.nextInt(y) + x;

        String valorFinal = Integer.toString(valor);
        textoCalculo.setText(valorFinal);
    }
}
